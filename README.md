The project includes utilities that help to protect against the <a rel="noopener noreferrer" href="https://www.owasp.org/index.php/Top_10-2017_A4-XML_External_Entities_(XXE)" target="_blank">OWASP A4 XML External Entities (XXE) attack</a>.

Created by <a rel="noopener noreferrer" href="www.linkedin.com/in/furmanmichael/" target="_blank">Michael Furman</a> and <a rel="noopener noreferrer" href="www.linkedin.com/in/anat-mazar" target="_blank">Anat Mazar</a>.

Read <a rel="noopener noreferrer" href="https://ultimatesecurity.pro/post/xxe-meetup/" target="_blank">the blog</a> for details.

The first utility is <a rel="noopener noreferrer" href="https://gitlab.com/ultimatesecpro/xxeprotection/-/blob/master/src/main/java/org/ultimatesecpro/xxeprotection/SafeDocumentBuilderFactory.java" target="_blank">SafeDocumentBuilderFactory</a>.
It is the implementation of <a rel="noopener noreferrer" href="https://cheatsheetseries.owasp.org/cheatsheets/XML_External_Entity_Prevention_Cheat_Sheet.html#jaxp-documentbuilderfactory-saxparserfactory-and-dom4j" target="_blank">javax.xml.parsers.DocumentBuilderFactory</a> that prevents the XXE attack.

The second utility is <a rel="noopener noreferrer" href="https://gitlab.com/ultimatesecpro/xxeprotection/-/blob/master/src/main/java/org/ultimatesecpro/xxeprotection/SafeDocumentBuilderFactoryImpl.java" target="_blank">SafeDocumentBuilderFactoryImpl</a>.
It is the implementation of <a rel="noopener noreferrer" href="https://cheatsheetseries.owasp.org/cheatsheets/XML_External_Entity_Prevention_Cheat_Sheet.html#jaxp-documentbuilderfactory-saxparserfactory-and-dom4j" target="_blank">javax.xml.parsers.DocumentBuilderFactory</a> that prevents the XXE attack via System Properties. 

The third utility is <a rel="noopener noreferrer" href="https://gitlab.com/ultimatesecpro/xxeprotection/-/blob/master/src/main/java/org/ultimatesecpro/xxeprotection/SafeSource.java" target="_blank">SafeSource</a>.
It is the implementation of <a rel="noopener noreferrer" href="https://cheatsheetseries.owasp.org/cheatsheets/XML_External_Entity_Prevention_Cheat_Sheet.html#jaxb-unmarshaller " target="_blank">JAXB Unmarshaller</a> that prevents the XXE attack. 
package org.ultimatesecpro.xxeprotection;


import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Source;
import javax.xml.transform.sax.SAXSource;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.io.StringReader;

/**
 *
 * @license
 * Copyright 2018 Michael Furman https://ultimatesecurity.pro
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * Implementation of javax.xml.transform.Source that prevents OWASP A4 XML External Entities
 * See for details https://www.owasp.org/index.php/XML_External_Entity_(XXE)_Prevention_Cheat_Sheet#JAXB_Unmarshaller
 *
 * Example 1
 * instead of
 * u.unmarshal(new StringReader(xml))
 * use
 * Source xmlSource = SafeSource.newInstanceFromXmlContent(xml);
 * u.unmarshal(xmlSource);
 *
 * Example 2
 * instead of
 * u.unmarshal(reader);
 * use
 * u.unmarshal(SafeSource.newInstanceFromReader(reader));
 *
 * Example 3
 * instead of
 * u.unmarshal(new File(filePath));
 * use
 * Source xmlSource = SafeSource.newInstanceFromFilePath(filePath);
 * u.unmarshal(xmlSource);
 *
 *
 * @author Michael Furman
 */

public class SafeSource {

    public static Source newInstanceFromXmlContent(String xml) throws SAXException, ParserConfigurationException {
        return newInstanceFromFilepathInputSource(new InputSource(new StringReader(xml)));
    }

    public static Source newInstanceFromFilePath(String filePath) throws SAXException, ParserConfigurationException, FileNotFoundException {
        return newInstanceFromFilepathInputSource(new InputSource(new FileReader(filePath)));
    }

    public static Source newInstanceFromReader(Reader reader) throws SAXException, ParserConfigurationException, FileNotFoundException {
        return newInstanceFromFilepathInputSource(new InputSource(reader));
    }

    private static Source newInstanceFromFilepathInputSource(InputSource inputSource) throws SAXException, ParserConfigurationException {
        SAXParserFactory spf = SAXParserFactory.newInstance();
        spf.setFeature("http://xml.org/sax/features/external-general-entities", false);
        spf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
        spf.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
        spf.setNamespaceAware(true);
        return new SAXSource(spf.newSAXParser().getXMLReader(), inputSource);
    }

}

package org.ultimatesecpro.xxeprotection;


import com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl;

import javax.xml.parsers.ParserConfigurationException;

/**
 *
 * @license
 * Copyright 2020 Michael Furman https://ultimatesecurity.pro
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Implementation of javax.xml.parsers.DocumentBuilderFactory that prevents OWASP A4 XML External Entities
 * See for details https://www.owasp.org/index.php/XML_External_Entity_(XXE)_Prevention_Cheat_Sheet#JAXP_DocumentBuilderFactory.2C_SAXParserFactory_and_DOM4J
 *
 * Usage: Add new System Properties in a configuration file:
 *
 * -Djavax.xml.parsers.DocumentBuilderFactory=org.ultimatesecpro.xxeprotection.SafeDocumentBuilderFactoryImpl
 *
 * @author Anat Mazar
 */


public class SafeDocumentBuilderFactoryImpl extends DocumentBuilderFactoryImpl {

    public SafeDocumentBuilderFactoryImpl() {
        try {
            this.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
            this.setFeature("http://xml.org/sax/features/external-general-entities", false);
            this.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
            this.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
            this.setXIncludeAware(false);
            this.setExpandEntityReferences(false);
        } catch (ParserConfigurationException e) {
            // log exception
        }
    }
}
